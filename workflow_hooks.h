/*
 * Copyright (C) 1996-2002,2010,2016 Michael R. Elkins <me@mutt.org>
 *
 *     This program is free software; you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation; either version 2 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

/* workflow sub-hook names - possible values of message_hook_origin option */

/* use following vim command to update the documentation in
 * init.h (note it will corrupt your register "a")
 *

 * :/#define .*NONE.*(0)/+1|.,/^$/ yank a|e init.h|/.*\.dt 0.*NONE$/+1|.;/\.de$/-1 d|-1|put a|'[,']s/\v#define\s+(\w+)\s+\((\d+)\)/  ** .dt \2  .dd \1/|+1|d
 * 
 *
 * */
#define MUTT_HOOK_WORKFLOW_NONE                                 (0)
#define MUTT_MSGHOOK_WORKFLOW_DISPLAY_MESSAGE                   (1)
#define MUTT_MSGHOOK_WORKFLOW_PIPE_MESSAGE_SINGLE               (2)
#define MUTT_MSGHOOK_WORKFLOW_PIPE_MESSAGE_MIME_PARSE           (3)
#define MUTT_MSGHOOK_WORKFLOW_PIPE_MESSAGES_NO_SPLIT            (4)
#define MUTT_MSGHOOK_WORKFLOW_PIPE_MESSAGES_SPLIT               (5)
#define MUTT_MSGHOOK_WORKFLOW_SAVE_MESSAGE                      (6)
#define MUTT_MSGHOOK_WORKFLOW_SAVE_MESSAGES_SAVE_DEFAULTS       (7)
#define MUTT_MSGHOOK_WORKFLOW_SAVE_MESSAGES                     (9)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_EDIT_FROM               (10)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_EDIT_TO                 (11)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_EDIT_BCC                (12)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_EDIT_CC                 (13)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_EDIT_SUBJECT            (14)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_EDIT_REPLY_TO           (15)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_EDIT_FCC                (16)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_EDIT_MESSAGE            (17)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_EDIT_HEADERS            (18)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_ATTACH_KEY              (19)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_ATTACH_FILE             (20)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_ATTACH_MESSAGE          (21)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_DELETE                  (22)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_TOGGLE_RECODE           (23)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_EDIT_DESCRIPTION        (24)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_UPDATE_ENCODING         (25)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_EDIT_TYPE               (26)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_EDIT_ENCODING           (27)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_EDIT_FILE               (28)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_RENAME_FILE             (29)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_NEW_MIME                (30)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_EDIT_MIME               (31)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_OP_FILTER               (32)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_PGP_MENU                (33)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_SMIME_MENU              (34)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_MIX                     (35)
#define MUTT_SEND2HOOK_WORKFLOW_COMPOSE_AUTOCRYPT_MENU          (36)
#define MUTT_MSGHOOK_WORKFLOW_VIEW_ATTACHMENTS                  (37)
#define MUTT_MSGHOOK_WORKFLOW_INCLUDE_FORWARD                   (38)
#define MUTT_MSGHOOK_WORKFLOW_INLINE_FORWARD_ATTACHMENTS        (39)
#define MUTT_MSGHOOK_WORKFLOW_INCLUDE_REPLY                     (40)
#define MUTT_REPLYHOOK_WORKFLOW_PRE_INIT                        (41)
#define MUTT_SENDHOOK_WORKFLOW_PRE_INIT                         (42)
#define MUTT_SEND2HOOK_WORKFLOW_PRE_INIT                        (43)
#define MUTT_SEND2HOOK_WORKFLOW_PRE_COMPOSE                     (44)

/* empty line after last constant is needed, see
 * lisp_get_message_hook_origin */
