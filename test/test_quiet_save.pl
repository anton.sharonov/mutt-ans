#!/usr/bin/perl -w
use 5.010;
use strict;

# assume running from within ./test folder in source distribution
my $mutt_exe = `realpath ../mutt`;
chomp $mutt_exe;
die ("mutt executable is not found at path $mutt_exe") if (!-x $mutt_exe);
my $default_timeout_sec = 10;
my @result = ();
my $i = undef;
my $right = undef; # means "right side hand" of check_equal()
my $expected = undef;
my $tmp_subdir_name = "tmp4test";

my $working_dir = `realpath ./$tmp_subdir_name`;
chomp $working_dir;

my $screenrc_fname = "$working_dir/screenrc4test";
my $muttrc_fname = "$working_dir/muttrc4test";
my $message_hook_debug_fname = "$working_dir/message_hook_debug";

my $hook_execution_log_fname = "$working_dir/hook_execution.log";
my $sent_mbx_fname = "$working_dir/sent";
my $fake_send_mail_fname = "$working_dir/fake_send_mail";
my $pipe_default_prog_fname = "$working_dir/pipe_default_prog";
my $pipe_default_prog_log_fname = "$working_dir/pipe_default_prog.log";
my $m1_fname = "$working_dir/m1";
my $m2_fname = "$working_dir/m2";

### working dir has to be empty in the beginning
# rename away to have the last run still available for reference
# purposes
if (-d $working_dir) {
  &rotate_file_or_dir($working_dir);
}

mkdir $working_dir;

### create screenrc for test
my $screenrc_content = <<'END_SCREENRC';
attrcolor b ".I"
termcapinfo xterm 'Co#256:AB=\E[48;5;%dm:AF=\E[38;5;%dm'
shell /bin/bash
startup_message off
defscrollback 5000
screen -t 0_test_scenario
END_SCREENRC

open FD_SCREENRC, ">$screenrc_fname" or die ("error open for ovewriting $screenrc_fname: $!");
print FD_SCREENRC $screenrc_content;
close FD_SCREENRC;

### create muttrc for test
my $muttrc_content = <<"END_MUTTRC";
# minimalistic muttrc for test
#set sendmail="/usr/bin/cat -"
set sendmail="$fake_send_mail_fname"
set record="$sent_mbx_fname";
set realname = "Test Robot"
set from = "test.robot\@somewhere.com"
message-hook . 'source $working_dir/message_hook_debug'
END_MUTTRC

open FD_MUTTRC, ">$muttrc_fname" or die ("error open for ovewriting $muttrc_fname: $!");
print FD_MUTTRC $muttrc_content;
close FD_MUTTRC;

### create message_hook_debug
my $message_hook_debug_content = <<'END_MESSAGE_HOOK_DEBUG';
set my_ctm=`date +%Y-%m-%d__%H:%M:%S`
set my_hook_is_suitable='no'

# set re-usable conditional to make further processing shorter
run (if (or (equal (get-message-hook-origin-name) 'MUTT_MSGHOOK_WORKFLOW_DISPLAY_MESSAGE') \
            (equal (get-message-hook-origin-name) 'NOT_EXIST_YET_SOME_OTHER_HOOK')) \
    (concat 'set my_hook_is_suitable=yes'))

# prepare global environment to call pipe-message
run (if (equal $my_hook_is_suitable 'yes') \
      (concat \
        'set wait_key = no;' \
    ) )

# do schedule call really
run (if (equal $my_hook_is_suitable 'yes') \
      (concat \
        'push <pipe-message>${PIPE_DEFAULT_PROG}<enter>;' \
    ) )

# do protocol if the hook execution was suitable
run (if (equal $my_hook_is_suitable 'yes') \
      (concat \
        'set my_diag_dummy=`echo "' $my_ctm ' pipe-message scheduled for execution from origin=' \
        (get-message-hook-origin-name) ' => (' (get-message-hook-origin) ')' \
        '" >> ${HOOK_EXECUTION_LOG}`' \
    ) )

# do protocol if the hook execution was not suitable
run (if (not (equal $my_hook_is_suitable 'yes')) \
      (concat \
        'set my_diag_dummy=`echo "' $my_ctm ' skip message_hook origin=' \
        (get-message-hook-origin-name) ' => (' (get-message-hook-origin) ')' \
        '" >> ${HOOK_EXECUTION_LOG}`;'       \
    ) )
END_MESSAGE_HOOK_DEBUG

# replace placeholders with the needed values, since we don't want
# to deal with quoting of all $-things inside the hook code to
# use doc-here expansion
$message_hook_debug_content =~ s|\$\{PIPE_DEFAULT_PROG\}|$pipe_default_prog_fname|g;
$message_hook_debug_content =~ s|\$\{PIPE_DEFAULT_PROG_LOG\}|$pipe_default_prog_log_fname|g;
$message_hook_debug_content =~ s|\$\{HOOK_EXECUTION_LOG\}|$hook_execution_log_fname|g;

open FD_FILE4TST, ">$message_hook_debug_fname" or die ("error open for ovewriting $message_hook_debug_fname $!");
print FD_FILE4TST $message_hook_debug_content;
close FD_FILE4TST;

### create fake_send_mail script
my $fake_send_mail_content = <<'END_FAKE_SEND_MAIL';
#!/usr/bin/perl -w
use strict;
use 5.010;

say "fake_send_mail is invoked with:\n", join ", ", map { '"' . $_ . '"' } @ARGV;
while(<STDIN>) {
  print $_;
}
END_FAKE_SEND_MAIL

open FD_FILE4TST, ">$fake_send_mail_fname" or die ("error open for ovewriting $fake_send_mail_fname $!");
print FD_FILE4TST $fake_send_mail_content;
close FD_FILE4TST;
system ("chmod", "+x", $fake_send_mail_fname) == 0 or die ("chmod on $fake_send_mail_fname failed: $!");

### create pipe_default_prog script
my $pipe_default_prog_content = <<'END_PIPE_DEFAULT_PROG';
#!/usr/bin/perl -w
use strict;
use 5.010;

open MYFILE, ">>${PIPE_DEFAULT_PROG_LOG}";
print MYFILE "this is pipe call from Mutt @ localtime " . localtime() . "\n";
while(<>) {
  if (/^From:/ || /^Subject:/ || /^To:/ || /^Date:/) {
    print MYFILE "  " . $_;
  }
}
END_PIPE_DEFAULT_PROG

$pipe_default_prog_content =~ s|\$\{PIPE_DEFAULT_PROG_LOG\}|$pipe_default_prog_log_fname|g;

open FD_FILE4TST, ">$pipe_default_prog_fname" or die ("error open for ovewriting $pipe_default_prog_fname $!");
print FD_FILE4TST $pipe_default_prog_content;
close FD_FILE4TST;
system ("chmod", "+x", $pipe_default_prog_fname) == 0 or die ("chmod on $pipe_default_prog_fname failed: $!");

### check_old_session
sub check_old_session {
  my $old_session_check_cmd = "screen -list 0_test_scenario_sess | grep 0_test_sce";
  my $old_session = `$old_session_check_cmd`;
  if ($old_session =~ /0_test_scenario_sess/) {
    return $old_session;
  }
  return undef;
}

### check if there is a session from last time
  # old session is here, try to send graceful quit to mutt and
  # exit to shell

sub check_old_session_and_ask_nicely_to_finish {

  if (&check_old_session) {
    say ("old session found, sending graceful quit sequences...");
    &talk_to_mutt("q");
    &talk_to_mutt("exit^M");
    sleep 1;
    my $old_session = &check_old_session;
    if ($old_session) {
      die ("old session `$old_session' still there, please finish it manually");
    }
    say ("old session seems to have terminated when we asked in nice way.");
  }

}

&check_old_session_and_ask_nicely_to_finish;

### fork new screen session in background
# -d means "detached" session enforced
# -m means to "not attach to it"
system (qw{screen -d -m -S 0_test_scenario_sess -c},
    $screenrc_fname) == 0 or die ("error to fork screen with mutt in background");

### enforce path inside shell of new session
&talk_to_mutt("cd $working_dir^M");

### create bunch of new messages for test (will be left in
#"sent", since sendmail is fake in our config)
unlink $sent_mbx_fname;
for my $currentAbbrev ('A'..'C') {
  system ("echo \"fake mail body, Best regards\" | ".
    "$mutt_exe -F $muttrc_fname -x -s \"fake msg N_$currentAbbrev\" to.whom\@it.may.concern") == 0
    or die ("error to exec mutt -x to create fake msg N1");
}

### mv ./sent to ./m1
system ("mv", $sent_mbx_fname, $m1_fname) == 0 or die ("cannot mv $sent_mbx_fname to $m1_fname: $!");

### talk_to_mutt
sub talk_to_mutt {
  my ($stuff_string) = @_;

  system (qw{screen -S 0_test_scenario_sess -p 0 -c},
    $screenrc_fname, "-X", "stuff",
      "$stuff_string") == 0
    or die ("error to send command `$stuff_string' to mutt running in screen session");
}

### start mutt inside newly created session
&talk_to_mutt("$mutt_exe -F $muttrc_fname^M");

### change to the m1 folder
&talk_to_mutt("c$m1_fname^M");

### rotate_file_or_dir
sub rotate_file_or_dir {
  my ($log_fname) = @_;
  my $dtstamp=`date +%Y_%m_%d_%H_%M_%S`;
  chomp $dtstamp;
  system ("mv", $log_fname, "$log_fname.prior_to.$dtstamp");
}

### rotate_log
sub rotate_log {
  &rotate_file_or_dir(@_);
}

# clean the log in the beginning via renaming it
&rotate_log($pipe_default_prog_log_fname);

### iterate A, B, C
# search for each message in a row, open it in pager and
# immediately close it

for my $currentAbbrev ('A'..'C') {
  # navigate to the message in the index
  &talk_to_mutt("/fake msg N_$currentAbbrev^M");
  # open the message in the pager
  &talk_to_mutt("^M");
  # close pager
  &talk_to_mutt("q");
}

### check_equal
sub check_equal {
  my ($expected, $right, $msg) = @_;
  if ($right =~ /^\s*$expected\s*$/) {
    say "OK: $right";
  } else {
    die "$msg: expected: `$expected', found: `$right'";
  }
}

### wait_file_with_line_appear
sub wait_file_with_line_appear {
  my ($fname_to_wait, $line_regexp, $timeout_sec) = @_;
  if (!defined($timeout_sec)) {
    $timeout_sec = $default_timeout_sec;
  }
  my $btm = time;
  my $ctm = time;
  my $found = 0;
  while (!$found && (($ctm - $btm) < $timeout_sec)) {
    if (! -s $fname_to_wait) {
      say "`$fname_to_wait' file does not exists, will try 1 second later, btm=$btm, ctm=$ctm";
      sleep 1;
      $ctm = time;
      next; # try 1 second later
    }
    say "`$fname_to_wait' file exists, ctm=$ctm";
    open FD_TO_WAIT, "<$fname_to_wait" or die ("cannot open $fname_to_wait for read: $!");
    while (<FD_TO_WAIT>) {
      if (/$line_regexp/) {
        say "found `$line_regexp' inside `$fname_to_wait' file";
        $found = 1;
        last;
      }
    }
    close FD_TO_WAIT;
    if ($found) {
      last;
    }
    sleep 1; # try 1 second later
    $ctm = time;
  }

  if (!$found) {
    die ("timeout_sec $timeout_sec elapsed while"
      . " waiting for regexp `$line_regexp' to appear"
      . " inside `$fname_to_wait'");
  }
}

# check in the log file if we have all messages piped in the
# right order
&wait_file_with_line_appear($pipe_default_prog_log_fname, "^\\s*Subject: fake msg N_C\$");

@result = `grep Subject: $pipe_default_prog_log_fname`;
chomp @result;

$i = 0;
for my $currentAbbrev ('A'..'C') {
  my $right = $result[$i++]; 
  my $expected = "Subject: fake msg N_$currentAbbrev";
  &check_equal($expected, $right, "Subject in the position $i-1 do not match");
}

$right = scalar(@result);
$expected = 3;
&check_equal($expected, $right, "Different number of subjects found in the log");

say("all checks are passed OK. Terminating screen session (comment out the last command to be able to check the state of running mutt inside GNU screen)");
&check_old_session_and_ask_nicely_to_finish;
say("all done.");
